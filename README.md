## About project

Theme of hackathon was popularization of STEM.

Our idea was to create a social network where you could register your Organisation or local community and write blogs or organise events or you could participate as private person commenting on said blogs, view events and join organisations.

Unfortunately our idea was too big and database was too complex so we didn't have enough time to finish this project