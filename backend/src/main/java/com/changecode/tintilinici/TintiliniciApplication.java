package com.changecode.tintilinici;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TintiliniciApplication {

    public static void main(String[] args) {
        SpringApplication.run(TintiliniciApplication.class, args);
    }

}
