package com.changecode.tintilinici.controller;

import com.changecode.tintilinici.dto.BlogCommentDTO;
import com.changecode.tintilinici.dto.BlogDTO;
import com.changecode.tintilinici.dto.BlogReactionDTO;
import com.changecode.tintilinici.dto.ResponseDTO;
import com.changecode.tintilinici.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/auth/blog")
public class BlogController {

    private final BlogService blogService;

    @Autowired
    public BlogController(BlogService blogService) {
        this.blogService = blogService;
    }

    @PostMapping(path = "/create", produces = "application/json")
    public ResponseEntity<ResponseDTO> create(@RequestBody BlogDTO blogDTO){
        return ResponseEntity.ok(new ResponseDTO(blogService.create(blogDTO)));
    }

    @PostMapping(path="/comment/create", produces = "application/json")
    public ResponseEntity<ResponseDTO> addComment(@RequestBody  BlogCommentDTO blogCommentDTO){
        return ResponseEntity.ok(new ResponseDTO(blogService.addComment(blogCommentDTO)));
    }

    @PostMapping(path="/react", produces = "application/json")
    public ResponseEntity<ResponseDTO> addReaction(@RequestBody BlogReactionDTO blogReactionDTO){
        return ResponseEntity.ok(new ResponseDTO(blogService.addReaction(blogReactionDTO)));
    }
}
