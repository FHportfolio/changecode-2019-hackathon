package com.changecode.tintilinici.controller;

import com.changecode.tintilinici.dto.EventDTO;
import com.changecode.tintilinici.dto.ResponseDTO;
import com.changecode.tintilinici.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/auth/event")
public class EventController {

    @Autowired
    private EventService eventService;

    @PostMapping(path = "/create", produces = "application/json")
    public ResponseEntity<ResponseDTO> create(@RequestBody EventDTO eventDTO){
        return ResponseEntity.ok(new ResponseDTO(eventService.create(eventDTO)));
    }
}
