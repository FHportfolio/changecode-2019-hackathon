package com.changecode.tintilinici.controller;

import com.changecode.tintilinici.dto.GroupDTO;
import com.changecode.tintilinici.dto.ResponseDTO;
import com.changecode.tintilinici.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/auth/group")
public class GroupController {

    private final GroupService groupService;

    @Autowired
    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @PostMapping(path = "/create", produces = "application/json")
    public ResponseEntity<ResponseDTO> create(@RequestBody GroupDTO groupDTO){
        return ResponseEntity.ok(new ResponseDTO(groupService.create(groupDTO)));
    }
}
