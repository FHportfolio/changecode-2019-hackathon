package com.changecode.tintilinici.controller;

import com.changecode.tintilinici.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/image")
public class ImageController {

    private final ImageService imageService;

    @Autowired
    public ImageController(ImageService imageService) {
        this.imageService = imageService;
    }

    @GetMapping(path = "/user", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getUserProfileImage(@RequestParam Long userId){
        return ResponseEntity.ok(imageService.userProfile(userId));
    }

    @GetMapping(path = "/group/profile", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getGroupProfileImage(@RequestParam Long groupId){
        return ResponseEntity.ok(imageService.groupProfile(groupId));
    }

    @GetMapping(path = "/group/heading", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getGroupHeadingImage(@RequestParam Long groupId){
        return ResponseEntity.ok(imageService.groupHeading(groupId));
    }

    @GetMapping(path = "/blog", produces = MediaType.IMAGE_JPEG_VALUE)
    public ResponseEntity<byte[]> getBlogImage(@RequestParam Long blogId){
        return ResponseEntity.ok(imageService.blog(blogId));
    }
}
