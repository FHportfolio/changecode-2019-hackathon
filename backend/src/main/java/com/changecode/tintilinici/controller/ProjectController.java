package com.changecode.tintilinici.controller;

import com.changecode.tintilinici.dto.ProjectDTO;
import com.changecode.tintilinici.dto.ResponseDTO;
import com.changecode.tintilinici.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/auth/project")
public class ProjectController {

    private final ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @PostMapping(path = "/create", produces = "application/json")
    public ResponseEntity<ResponseDTO> create(@RequestBody ProjectDTO projectDTO){
        return ResponseEntity.ok(new ResponseDTO(projectService.create(projectDTO)));
    }
}
