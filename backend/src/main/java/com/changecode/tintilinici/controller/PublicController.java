package com.changecode.tintilinici.controller;

import com.changecode.tintilinici.dto.BlogDTO;
import com.changecode.tintilinici.dto.EventDTO;
import com.changecode.tintilinici.dto.GroupDTO;
import com.changecode.tintilinici.dto.ProjectDTO;
import com.changecode.tintilinici.service.BlogService;
import com.changecode.tintilinici.service.EventService;
import com.changecode.tintilinici.service.GroupService;
import com.changecode.tintilinici.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/public")
public class PublicController {

    private final EventService eventService;
    private final BlogService blogService;
    private final ProjectService projectService;
    private final GroupService groupService;


    @Autowired
    public PublicController(EventService eventService, BlogService blogService, ProjectService projectService, GroupService groupService) {
        this.eventService = eventService;
        this.blogService = blogService;
        this.projectService = projectService;
        this.groupService = groupService;
    }

    @GetMapping(path = "/events", produces = "application/json")
    public ResponseEntity<Page<EventDTO>> getEventsPageable(@RequestParam(required = false) String tag,@RequestParam int page,@RequestParam int size){
        return ResponseEntity.ok(eventService.getByTagSorted(tag, page, size, "fromDate", Sort.Direction.ASC));
    }

    @GetMapping(path = "/blogs", produces = "application/json")
    public ResponseEntity<Page<BlogDTO>> getBlogsPageable(@RequestParam(required = false) String tag, @RequestParam int page,@RequestParam int size){
        return ResponseEntity.ok(blogService.getByTagSorted(tag, page, size, "createdAt", Sort.Direction.ASC));
    }

    @GetMapping(path = "/projects", produces = "application/json")
    public ResponseEntity<Page<ProjectDTO>> getProjectsPageable(@RequestParam(name="tag", required = false) String tag,@RequestParam(name="page") int page,@RequestParam(name="size") int size){
        return ResponseEntity.ok(projectService.getByTagSorted(tag, page, size ));
    }

    @GetMapping(path = "/groups", produces = "application/json")
    public ResponseEntity<List<GroupDTO>> getTopGroups(){
        return ResponseEntity.ok(groupService.getTopGroups());
    }

    @GetMapping(path = "/event", produces = "application/json")
    public ResponseEntity<EventDTO> getEvent(@RequestParam Long id){
        return ResponseEntity.ok(eventService.getById(id));
    }

    @GetMapping(path = "/project", produces = "application/json")
    public ResponseEntity<ProjectDTO> getProject(@RequestParam Long id){
        return ResponseEntity.ok(projectService.getById(id));
    }

    @GetMapping(path = "/blog", produces = "application/json")
    public ResponseEntity<BlogDTO> getBlog(@RequestParam Long id){
        return ResponseEntity.ok(blogService.getById(id));
    }
}
