package com.changecode.tintilinici.controller;

import com.changecode.tintilinici.dto.GroupDTO;
import com.changecode.tintilinici.dto.ResponseDTO;
import com.changecode.tintilinici.dto.UserDTO;
import com.changecode.tintilinici.service.GroupService;
import com.changecode.tintilinici.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "/user")
public class UserController {

    private final UserService userService;
    private final GroupService groupService;


    @Autowired
    public UserController(UserService userService, GroupService groupService) {
        this.groupService = groupService;
        this.userService = userService;
    }

    @PostMapping(path = "/register")
    public ResponseEntity<ResponseDTO> register(@RequestBody UserDTO user){
        Long entityId = userService.create(user);

        return ResponseEntity.ok(new ResponseDTO(entityId));
    }

    @GetMapping(path="/group/following", produces = "application/json")
    public ResponseEntity<List<GroupDTO>> getFollowingGroups(){
        return ResponseEntity.ok(groupService.getGroupsFollowed());
    }
}
