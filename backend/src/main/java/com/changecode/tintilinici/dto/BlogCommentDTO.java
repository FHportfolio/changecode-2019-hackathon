package com.changecode.tintilinici.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class BlogCommentDTO implements Serializable {

    private static final long serialVersionUID = -1335541296300718000L;

    private Long id;

    private UserDTO user;

    private Long userId;

    private String email;

    private String body;

    private Long blogId;
}
