package com.changecode.tintilinici.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class BlogDTO implements Serializable {

    private static final long serialVersionUID = -4266135018704898339L;

    private Long id;

    private UserDTO user;

    private Long userId;

    private GroupDTO group;

    private Long groupId;

    private StemDTO stem;

    private Long stemId;

    private String title;

    private String body;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date createdAt;

    private List<BlogCommentDTO> blogComments;

    private String image;

    private byte[] imageData;

    private Integer upvoteCount;

    private Integer downvoteCount;
}
