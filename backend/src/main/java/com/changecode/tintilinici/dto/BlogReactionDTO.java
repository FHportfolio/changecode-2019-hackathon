package com.changecode.tintilinici.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BlogReactionDTO {

    private Long blogId;

    private Boolean isUpvote;
}
