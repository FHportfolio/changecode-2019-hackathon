package com.changecode.tintilinici.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@NoArgsConstructor
public class EventDTO implements Serializable {

    private static final long serialVersionUID = 2391168396542553969L;

    private Long id;

    private GroupDTO group;

    private Long groupId;

    private String title;

    private String description;

    private StemDTO stem;

    private Long stemId;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date fromDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date toDate;

    private Integer goingCount;
}
