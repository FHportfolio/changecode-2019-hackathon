package com.changecode.tintilinici.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
public class GroupDTO implements Serializable {

    private static final long serialVersionUID = 3536656565141830365L;

    private Long id;

    private String name;

    private String description;

    private StemDTO stem;

    private Long stemId;

    private String profileImage;

    private byte[] profileImageData;

    private String headerImage;

    private byte[] headerImageData;

    private Integer followersCount;

    private Integer membersCount;

    private List<ProjectDTO> projects;

    private List<EventDTO> events;

    private Double rating;
}
