package com.changecode.tintilinici.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GroupRequestDTO {

    private Long id;

    private String motivation;

    private UserDTO user;

    private Long userId;

    private GroupDTO group;

    private Long groupId;
}
