package com.changecode.tintilinici.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
public class ProjectDTO implements Serializable {

    private static final long serialVersionUID = -5619466603436789632L;

    private Long id;

    private String name;

    private String description;

    private String headerImage;

    private byte[] headerImageData;

    private Long groupId;

    private List<GroupDTO> groups;

    private StemDTO stem;

    private Long stemId;
}
