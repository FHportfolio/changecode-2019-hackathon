package com.changecode.tintilinici.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StemDTO implements Serializable {

    private static final long serialVersionUID = -7309183518502448580L;

    private Long id;

    private String code;
}
