package com.changecode.tintilinici.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class TagDTO implements Serializable {

    private static final long serialVersionUID = -984206021811807849L;

    private Long id;

    private String title;
}
