package com.changecode.tintilinici.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserDTO {

    private Long id;

    private String name;

    private String surname;

    private String email;

    private String password;

    private String profileImage;

    private byte[] profileImageData;
}
