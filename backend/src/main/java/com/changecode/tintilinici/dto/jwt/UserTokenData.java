package com.changecode.tintilinici.dto.jwt;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserTokenData {

    private Long id;
    private String email;
}
