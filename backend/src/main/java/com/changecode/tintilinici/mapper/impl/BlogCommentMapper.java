package com.changecode.tintilinici.mapper.impl;

import com.changecode.tintilinici.dto.BlogCommentDTO;
import com.changecode.tintilinici.dto.UserDTO;
import com.changecode.tintilinici.mapper.Mapper;
import com.changecode.tintilinici.model.Blog;
import com.changecode.tintilinici.model.BlogComment;
import com.changecode.tintilinici.model.User;
import com.changecode.tintilinici.repository.BlogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class BlogCommentMapper implements Mapper<BlogComment, BlogCommentDTO> {

    private final BlogRepository blogRepository;

    @Autowired
    public BlogCommentMapper(@Lazy BlogRepository blogRepository) {
        this.blogRepository = blogRepository;
    }

    @Override
    public BlogCommentDTO entityToDto(BlogComment entity) {
        BlogCommentDTO dto = new BlogCommentDTO();

        dto.setId(entity.getId());
        dto.setBody(entity.getBody());
        dto.setEmail(entity.getEmail());


        User userEntity = entity.getUser();

        if(userEntity != null){
            UserDTO userDTO = new UserDTO();
            userDTO.setName(userEntity.getName());
            userDTO.setSurname(userEntity.getSurname());
            dto.setUser(userDTO);
        }

        return dto;
    }

    @Override
    public BlogComment dtoToEntity(BlogCommentDTO dto) {
        BlogComment entity = new BlogComment();

        entity.setBody(dto.getBody());
        entity.setEmail(dto.getEmail());

        Blog blog = blogRepository.findById(entity.getId()).orElseThrow(EntityNotFoundException::new);
        entity.setBlog(blog);

        return entity;
    }
}
