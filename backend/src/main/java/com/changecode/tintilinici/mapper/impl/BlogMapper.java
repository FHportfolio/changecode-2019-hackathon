package com.changecode.tintilinici.mapper.impl;

import com.changecode.tintilinici.dto.BlogDTO;
import com.changecode.tintilinici.dto.GroupDTO;
import com.changecode.tintilinici.dto.StemDTO;
import com.changecode.tintilinici.dto.UserDTO;
import com.changecode.tintilinici.mapper.Mapper;
import com.changecode.tintilinici.model.Blog;
import com.changecode.tintilinici.model.BlogComment;
import com.changecode.tintilinici.model.Group;
import com.changecode.tintilinici.model.Stem;
import com.changecode.tintilinici.model.User;
import com.changecode.tintilinici.repository.GroupRepository;
import com.changecode.tintilinici.repository.StemRepository;
import com.changecode.tintilinici.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class BlogMapper implements Mapper<Blog, BlogDTO> {

    private final BlogCommentMapper blogCommentMapper;
    private final UserRepository userRepository;
    private final GroupRepository groupRepository;
    private final StemRepository stemRepository;

    @Autowired
    public BlogMapper(@Lazy BlogCommentMapper blogCommentMapper, @Lazy UserRepository userRepository, @Lazy GroupRepository groupRepository, @Lazy StemRepository stemRepository) {
        this.blogCommentMapper = blogCommentMapper;
        this.userRepository = userRepository;
        this.groupRepository = groupRepository;
        this.stemRepository = stemRepository;
    }


    @Override
    public BlogDTO entityToDto(Blog entity) {
        BlogDTO dto = new BlogDTO();

        dto.setBody(entity.getBody());
        dto.setTitle(entity.getTitle());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setGroupId(entity.getGroupId());
        dto.setId(entity.getId());
        dto.setImage("/image/blog?blogId=" + entity.getId());

        Group groupEntity = entity.getGroup();
        GroupDTO groupDTO = new GroupDTO();
        groupDTO.setName(groupEntity.getName());

        dto.setGroup(groupDTO);

        User userEntity = entity.getUser();
        UserDTO userDTO = new UserDTO();
        userDTO.setId(userEntity.getId());
        userDTO.setName(userEntity.getName());
        userDTO.setSurname(userEntity.getSurname());

        dto.setUser(userDTO);
        dto.setUserId(userEntity.getId());

        Stem stemEntity = entity.getStem();
        dto.setStem(new StemDTO(stemEntity.getId(), stemEntity.getCode()));
        dto.setStemId(stemEntity.getId());
        dto.setUpvoteCount(entity.getUpvoteCount());
        dto.setDownvoteCount(entity.getDownvoteCount());

        Set<BlogComment> comments = entity.getBlogComments();
        if (comments != null) {
            dto.setBlogComments(comments.stream().map(blogCommentMapper::entityToDto).collect(Collectors.toList()));
        }

        return dto;
    }

    @Override
    public Blog dtoToEntity(BlogDTO dto) {
        Blog entity = new Blog();

        entity.setBody(dto.getBody());
        entity.setTitle(dto.getTitle());
        entity.setCreatedAt(new Date());
        entity.setEdited(new Date());
        entity.setGroup(groupRepository.findById(dto.getGroupId()).orElseThrow(EntityNotFoundException::new));
        entity.setStem(stemRepository.findById(dto.getStemId()).orElseThrow(EntityNotFoundException::new));
        entity.setUser(userRepository.findByEmail(
                SecurityContextHolder
                        .getContext()
                        .getAuthentication()
                        .getName()).get()
        );

        if (dto.getImageData() != null) {
            entity.setImage(dto.getImageData());
        }

        return entity;
    }
}
