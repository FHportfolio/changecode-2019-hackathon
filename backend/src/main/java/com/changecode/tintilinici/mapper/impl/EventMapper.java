package com.changecode.tintilinici.mapper.impl;

import com.changecode.tintilinici.dto.EventDTO;
import com.changecode.tintilinici.dto.GroupDTO;
import com.changecode.tintilinici.dto.StemDTO;
import com.changecode.tintilinici.mapper.Mapper;
import com.changecode.tintilinici.mapper.util.TagParser;
import com.changecode.tintilinici.model.Event;
import com.changecode.tintilinici.model.Group;
import com.changecode.tintilinici.model.Stem;
import com.changecode.tintilinici.model.Tag;
import com.changecode.tintilinici.repository.GroupRepository;
import com.changecode.tintilinici.repository.StemRepository;
import com.changecode.tintilinici.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Component
public class EventMapper implements Mapper<Event, EventDTO> {

    private final GroupRepository groupRepository;
    private final StemRepository stemRepository;
    private final TagRepository tagRepository;

    @Autowired
    public EventMapper(@Lazy GroupRepository groupRepository, @Lazy StemRepository stemRepository, @Lazy TagRepository tagRepository) {
        this.groupRepository = groupRepository;
        this.stemRepository = stemRepository;
        this.tagRepository = tagRepository;
    }


    @Override
    public EventDTO entityToDto(Event entity) {
        EventDTO dto = new EventDTO();

        dto.setTitle(entity.getTitle());
        dto.setDescription(entity.getDescription());
        dto.setFromDate(entity.getFromDate());
        dto.setToDate(entity.getToDate());
        dto.setGoingCount(entity.getGoingCount());

        Group groupEntity = entity.getGroup();
        GroupDTO groupDTO = new GroupDTO();
        groupDTO.setName(groupEntity.getName());
        groupDTO.setId(groupEntity.getId());
        dto.setGroup(groupDTO);
        dto.setGroupId(groupEntity.getId());

        Stem stemEntity = entity.getStem();
        dto.setStem(new StemDTO(stemEntity.getId(), stemEntity.getCode()));

        dto.setStemId(stemEntity.getId());
        return dto;
    }

    @Override
    public Event dtoToEntity(EventDTO dto) {
        Event event = new Event();

        event.setDescription(dto.getDescription());
        event.setFromDate(dto.getFromDate());
        event.setToDate(dto.getToDate());
        event.setGoingCount(0);
        event.setGroup(groupRepository.findById(dto.getGroupId()).orElseThrow(EntityNotFoundException::new));

        Stem stemEntity = stemRepository.findById(dto.getStemId()).orElseThrow(EntityNotFoundException::new);
        event.setStem(stemEntity);

        Set<String> tagNames = TagParser.parseDescription(dto.getDescription());
        tagNames.addAll(TagParser.parseStem(stemEntity.getCode()));

        Set<Tag> tags = new HashSet<>();
        for(String tagName: tagNames){
            Optional<Tag> tagEntity = tagRepository.findByTitle(tagName);
            if(tagEntity.isPresent()){
                tags.add(tagEntity.get());
            } else {
                Tag newTag = new Tag();
                newTag.setTitle(tagName);
                tags.add(newTag);
            }
        }
        event.setTags(tags);

        return event;
    }
}
