package com.changecode.tintilinici.mapper.impl;

import com.changecode.tintilinici.dto.EventDTO;
import com.changecode.tintilinici.dto.GroupDTO;
import com.changecode.tintilinici.dto.StemDTO;
import com.changecode.tintilinici.mapper.Mapper;
import com.changecode.tintilinici.model.Group;
import com.changecode.tintilinici.model.Stem;
import com.changecode.tintilinici.model.User;
import com.changecode.tintilinici.repository.StemRepository;
import com.changecode.tintilinici.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class GroupMapper implements Mapper<Group, GroupDTO> {

    private final ProjectMapper projectMapper;
    private final StemRepository stemRepository;
    private final UserRepository userRepository;

    @Autowired
    public GroupMapper(@Lazy ProjectMapper projectMapper, @Lazy StemRepository stemRepository, @Lazy UserRepository userRepository) {
        this.projectMapper = projectMapper;
        this.stemRepository = stemRepository;
        this.userRepository = userRepository;
    }


    @Override
    public GroupDTO entityToDto(Group entity){
        GroupDTO dto = new GroupDTO();

        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());
        dto.setFollowersCount(entity.getFollowersCount());
        dto.setMembersCount(entity.getMembersCount());
        dto.setProjects(entity.getProjects().stream().map(projectMapper::entityToDto).collect(Collectors.toList()));
        dto.setRating(entity.getRating());
        dto.setEvents(entity.getEvents().stream().map(e -> {
            EventDTO d = new EventDTO();

            d.setTitle(e.getTitle());
            d.setDescription(e.getDescription());
            d.setFromDate(e.getFromDate());
            d.setToDate(e.getToDate());
            d.setGoingCount(e.getGoingCount());
            d.setGroupId(e.getGroupId());

            Stem stemEntity = e.getStem();
            d.setStem(new StemDTO(stemEntity.getId(), stemEntity.getCode()));
            d.setStemId(stemEntity.getId());

            return d;
        }).collect(Collectors.toList()));

        if(entity.getHeaderImage() != null)  dto.setHeaderImage("/image/group/header?groupId=" + entity.getId());
        if(entity.getProfileImage() != null) dto.setProfileImage("/image/group/profile?Id=" + entity.getId());

        return dto;
    }

    @Override
    public Group dtoToEntity(GroupDTO dto) {
        Group entity = new Group();

        entity.setDescription(dto.getDescription());
        entity.setFollowersCount(1);
        entity.setMembersCount(1);

        entity.setHeaderImage(dto.getHeaderImageData());
        entity.setProfileImage(dto.getProfileImageData());
        entity.setStem(stemRepository.findById(dto.getStemId()).orElseThrow(EntityNotFoundException::new));

        User currentUser = userRepository.findByEmail(
                SecurityContextHolder
                        .getContext()
                        .getAuthentication()
                        .getName()).get();

        Set<User> shortSet = new HashSet<>();
        shortSet.add(currentUser);

        entity.setMembers(shortSet);
        entity.setAdmins(shortSet);
        entity.setFollowers(shortSet);

        return entity;
    }
}
