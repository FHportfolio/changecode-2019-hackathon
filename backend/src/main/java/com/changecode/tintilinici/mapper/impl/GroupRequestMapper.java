package com.changecode.tintilinici.mapper.impl;

import com.changecode.tintilinici.dto.GroupRequestDTO;
import com.changecode.tintilinici.mapper.Mapper;
import com.changecode.tintilinici.model.GroupRequest;
import com.changecode.tintilinici.model.User;
import com.changecode.tintilinici.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class GroupRequestMapper implements Mapper<GroupRequest, GroupRequestDTO> {

    private final UserMapper userMapper;
    private final UserRepository userRepository;

    @Autowired
    public GroupRequestMapper(@Lazy UserMapper userMapper, @Lazy UserRepository userRepository) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
    }

    @Override
    public GroupRequestDTO entityToDto(GroupRequest entity) {
        GroupRequestDTO dto = new GroupRequestDTO();

        dto.setId(entity.getId());
        dto.setMotivation(entity.getMotivation());
        dto.setGroupId(entity.getGroupId());

        dto.setUser(userMapper.entityToDto(entity.getUser()));

        return dto;
    }

    @Override
    public GroupRequest dtoToEntity(GroupRequestDTO dto) {
        GroupRequest groupRequest = new GroupRequest();

        groupRequest.setMotivation(dto.getMotivation());

        User currentUser = userRepository.findByEmail(
                SecurityContextHolder
                        .getContext()
                        .getAuthentication()
                        .getName()).get();
        groupRequest.setUser(currentUser);

        return groupRequest;
    }
}
