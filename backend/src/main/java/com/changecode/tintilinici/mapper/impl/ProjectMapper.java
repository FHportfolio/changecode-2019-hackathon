package com.changecode.tintilinici.mapper.impl;

import com.changecode.tintilinici.dto.GroupDTO;
import com.changecode.tintilinici.dto.ProjectDTO;
import com.changecode.tintilinici.dto.StemDTO;
import com.changecode.tintilinici.mapper.Mapper;
import com.changecode.tintilinici.mapper.util.TagParser;
import com.changecode.tintilinici.model.Group;
import com.changecode.tintilinici.model.Project;
import com.changecode.tintilinici.model.Stem;
import com.changecode.tintilinici.model.Tag;
import com.changecode.tintilinici.repository.GroupRepository;
import com.changecode.tintilinici.repository.StemRepository;
import com.changecode.tintilinici.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class ProjectMapper implements Mapper<Project, ProjectDTO> {

    private final StemRepository stemRepository;
    private final GroupRepository groupRepository;
    private final TagRepository tagRepository;

    @Autowired
    public ProjectMapper(@Lazy StemRepository stemRepository, @Lazy GroupRepository groupRepository, @Lazy TagRepository tagRepository) {
        this.stemRepository = stemRepository;
        this.groupRepository = groupRepository;
        this.tagRepository = tagRepository;
    }


    @Override
    public ProjectDTO entityToDto(Project entity) {
        ProjectDTO dto = new ProjectDTO();

        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setDescription(entity.getDescription());

        Stem stemEntity = entity.getStem();
        dto.setStem(new StemDTO(stemEntity.getId(), stemEntity.getCode()));

        dto.setHeaderImage("/image/project?projectId=" + entity.getId());
        dto.setGroups(entity.getGroups().stream().map(e -> {
            GroupDTO g = new GroupDTO();
            g.setId(e.getId());
            g.setName(e.getName());
            g.setProfileImage("/image/user?userId=" + e.getId());

            return g;
        }).collect(Collectors.toList()));

        return dto;
    }

    @Override
    public Project dtoToEntity(ProjectDTO dto){
        Project entity = new Project();

        entity.setDescription(dto.getDescription());
        entity.setName(dto.getName());
        entity.setHeaderImage(dto.getHeaderImageData());

        Stem stemEntity = stemRepository.findById(dto.getStemId()).orElseThrow(EntityNotFoundException::new);
        entity.setStem(stemEntity);

        Set<Group> groups = new HashSet<>();
        groups.add(groupRepository.findById(dto.getGroupId()).orElseThrow(EntityNotFoundException::new));
        entity.setGroups(groups);

        Set<String> tagNames = TagParser.parseDescription(dto.getDescription());
        tagNames.addAll(TagParser.parseStem(stemEntity.getCode()));

        Set<Tag> tags = new HashSet<>();
        for(String tagName: tagNames){
            Optional<Tag> tagEntity = tagRepository.findByTitle(tagName);
            if(tagEntity.isPresent()){
                tags.add(tagEntity.get());
            } else {
                Tag newTag = new Tag();
                newTag.setTitle(tagName);
                tags.add(newTag);
            }
        }

        entity.setTags(tags);

        return entity;
    }
}
