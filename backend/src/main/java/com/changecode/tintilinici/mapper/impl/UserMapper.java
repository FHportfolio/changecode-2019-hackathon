package com.changecode.tintilinici.mapper.impl;

import com.changecode.tintilinici.dto.UserDTO;
import com.changecode.tintilinici.mapper.Mapper;
import com.changecode.tintilinici.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserMapper implements Mapper<User, UserDTO> {

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserMapper(@Lazy PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDTO entityToDto(User entity) {
        UserDTO dto = new UserDTO();

        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setSurname(entity.getSurname());
        dto.setEmail(entity.getEmail());
        dto.setProfileImage("/image/user?userId="+entity.getId());

        return dto;
    }

    @Override
    public User dtoToEntity(UserDTO dto){
        User user = new User();

        user.setName(dto.getName());
        user.setSurname(dto.getSurname());
        user.setEmail(dto.getEmail());
        user.setPasswordHash(passwordEncoder.encode(dto.getPassword()));
        user.setIsAdmin(false);
        user.setProfileImage(dto.getProfileImageData());

        return user;
    }
}
