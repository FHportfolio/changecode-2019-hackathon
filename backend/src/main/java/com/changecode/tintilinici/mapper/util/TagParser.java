package com.changecode.tintilinici.mapper.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TagParser {

    public static Set<String> parseDescription(String description){
        Set<String> tags = new HashSet<>();

        boolean isTag = false;
        StringBuilder sb = new StringBuilder();
        for(char c: description.toCharArray()){
            if(c == '#') {
                isTag = true;
            }

            if(isTag && Character.isSpaceChar(c)) {
                isTag = false;
                if(sb.length() != 0){
                    tags.add(sb.toString());
                    sb = new StringBuilder();
                }
            }

            if(isTag){
                sb.append(c);
            }
        }
        return tags;
    }

    public static Set<String> parseStem(String code){
        Set<String> tags = new HashSet<>();

        for(char c : code.toCharArray()){
            if(c == '*') continue;

            switch (c){
                case 's':
                    tags.add("Science");
                    break;
                case 't':
                    tags.add("Technology");
                    break;
                case 'e':
                    tags.add("Engineering");
                    break;
                case 'm':
                    tags.add("Mathematics");
                    break;
            }
         }

        return tags;
    }
}
