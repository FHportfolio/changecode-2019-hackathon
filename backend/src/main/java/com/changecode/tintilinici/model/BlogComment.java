package com.changecode.tintilinici.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "BLOG_COMMENTS")
@NoArgsConstructor
@Data
public class BlogComment implements Serializable {

    private static final long serialVersionUID = -4464669658263004269L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false, nullable = false)
    @Setter(value = AccessLevel.PRIVATE)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_ID")
    private User user;

    @Column(name = "USER_ID", updatable = false,  insertable = false)
    @Setter(value = AccessLevel.PRIVATE)
    private Long userId;

    @Column(name = "CREATOR_EMAIL", nullable = false)
    private String email;

    @Column(name = "BODY", nullable = false)
    private String body;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "BLOG_ID", nullable = false)
    private Blog blog;

    @Column(name = "BLOG_ID", updatable = false,  insertable = false)
    @Setter(value = AccessLevel.PRIVATE)
    private Long blogId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BlogComment that = (BlogComment) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
