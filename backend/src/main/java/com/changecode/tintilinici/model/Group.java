package com.changecode.tintilinici.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "GROUPS")
@NoArgsConstructor
@Data
public class Group implements Serializable {

    private static final long serialVersionUID = 6039273419189537411L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false, nullable = false)
    @Setter(value = AccessLevel.PRIVATE)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STEM_ID", nullable = false)
    private Stem stem;

    @Column(name = "STEM_ID", updatable = false,  insertable = false)
    @Setter(value = AccessLevel.PRIVATE)
    private Long stemId;

    @Column(name = "FOLLOWER_COUNT", nullable = false)
    private Integer followersCount;

    @Column(name = "MEMBERS_COUNT", nullable = false)
    private Integer membersCount;


    @Column(name = "PROFILE_IMAGE")
    private byte[] profileImage;

    @Column(name = "RATING", nullable = false)
    private Double rating = 0.0;

    @Column(name = "HEADER_IMAGE")
    private byte[] headerImage;

    @JoinTable(
            name = "GROUP_MEMBERS",
            joinColumns = @JoinColumn(
                    name = "GROUP_ID",
                    referencedColumnName = "id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "USER_ID",
                    referencedColumnName = "id"
            )
    )
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<User> members;

    @JoinTable(
            name = "GROUP_ADMINS",
            joinColumns = @JoinColumn(
                    name = "GROUP_ID",
                    referencedColumnName = "id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "USER_ID",
                    referencedColumnName = "id"
            )
    )
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<User> admins;

    @JoinTable(
            name = "GROUP_FOLLOWERS",
            joinColumns = @JoinColumn(
                    name = "GROUP_ID",
                    referencedColumnName = "id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "USER_ID",
                    referencedColumnName = "id"
            )
    )
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<User> followers;

    @ManyToMany(mappedBy = "groups", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Project> projects;

    @OneToMany(mappedBy = "group",  orphanRemoval = true, cascade = CascadeType.ALL)
    private Set<Event> events;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return Objects.equals(id, group.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
