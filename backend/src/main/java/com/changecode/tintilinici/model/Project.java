package com.changecode.tintilinici.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "PROJECTS")
@NoArgsConstructor
@Data
public class Project implements Serializable {

    private static final long serialVersionUID = -2447779421266317707L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false, nullable = false)
    @Setter(value = AccessLevel.PRIVATE)
    private Long id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "HEADER_IMAGE")
    private byte[] headerImage;

    @JoinTable(
            name = "PROJECTS_GROUPS",
            joinColumns = @JoinColumn(
                    name = "PROJECT_ID",
                    referencedColumnName = "id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "GROUP_ID",
                    referencedColumnName = "id"
            )
    )
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Group> groups;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToMany(mappedBy = "projects", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private Set<Tag> tags;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "STEM_ID", nullable = false)
    private Stem stem;

    @Column(name = "STEM_ID", insertable = false, updatable = false)
    @Setter(value = AccessLevel.PRIVATE)
    private Long stemId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return Objects.equals(id, project.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
