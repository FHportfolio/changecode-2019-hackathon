package com.changecode.tintilinici.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "STEM")
@NoArgsConstructor
@Data
public class Stem implements Serializable {

    private static final long serialVersionUID = 4217663061173135810L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", updatable = false, nullable = false)
    @Setter(value = AccessLevel.PRIVATE)
    private Long id;

    @Column(name = "CODE", nullable = false)
    private String code;

    @OneToMany(mappedBy = "stem", orphanRemoval = true)
    private Set<Blog> blogs;

    @OneToMany(mappedBy = "stem", orphanRemoval = true)
    private Set<Event> events;

    @OneToMany(mappedBy = "stem", orphanRemoval = true)
    private Set<Project> projects;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Stem stem = (Stem) o;
        return Objects.equals(id, stem.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
