package com.changecode.tintilinici.repository;

import com.changecode.tintilinici.model.BlogReaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlogReactionRepository extends JpaRepository<BlogReaction, Long> {
}
