package com.changecode.tintilinici.repository;

import com.changecode.tintilinici.model.Blog;
import com.changecode.tintilinici.model.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlogRepository extends JpaRepository<Blog, Long> {

    Page<Blog> findAllByGroupId(Long groupId, Pageable pageable);

    Page<Blog> findAll(Pageable pageable);

    Page<Blog> findAllByTagsContaining(Tag tag, Pageable pageable);
}
