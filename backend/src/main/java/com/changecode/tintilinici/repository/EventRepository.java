package com.changecode.tintilinici.repository;

import com.changecode.tintilinici.model.Event;
import com.changecode.tintilinici.model.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    Page<Event> findAllByFromDateAfter(Date date, Pageable pageable);

    Page<Event> findAllByFromDateAfterAndTagsContaining(Date date, Tag tag, Pageable pageable);
}
