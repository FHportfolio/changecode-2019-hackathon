package com.changecode.tintilinici.repository;

import com.changecode.tintilinici.model.GroupRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupRequestRepository extends JpaRepository<GroupRequest, Long> {
}
