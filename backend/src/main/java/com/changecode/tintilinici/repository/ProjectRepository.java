package com.changecode.tintilinici.repository;

import com.changecode.tintilinici.model.Project;
import com.changecode.tintilinici.model.Tag;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

    Page<Project> findAll(Pageable pageable);

    Page<Project> findAllByTagsContaining(Tag tag, Pageable pageable);
}
