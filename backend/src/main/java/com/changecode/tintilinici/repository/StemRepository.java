package com.changecode.tintilinici.repository;

import com.changecode.tintilinici.model.Stem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StemRepository extends JpaRepository<Stem,Long> {
}
