package com.changecode.tintilinici.repository;

import com.changecode.tintilinici.model.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {
    List<Tag> findTop5ByTitleStartsWith(String pattern);

    Optional<Tag> findByTitle(String tag);
}
