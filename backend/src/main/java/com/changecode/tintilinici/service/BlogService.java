package com.changecode.tintilinici.service;

import com.changecode.tintilinici.dto.BlogCommentDTO;
import com.changecode.tintilinici.dto.BlogDTO;
import com.changecode.tintilinici.dto.BlogReactionDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

public interface BlogService {

    BlogDTO getById(Long id);

    Long create(BlogDTO blogDTO);

    Page<BlogDTO> getByGroupIdSorted(Long groupId, int page, int pageSize, String sortedBy, Sort.Direction direction);

    Page<BlogDTO> getByTagSorted(String tag, int page, int pageSize, String sortedBy, Sort.Direction direction);

    Long addComment(BlogCommentDTO commentDTO);

    Long addReaction(BlogReactionDTO blogReactionDTO);

    //TODO other CRUD operations
}
