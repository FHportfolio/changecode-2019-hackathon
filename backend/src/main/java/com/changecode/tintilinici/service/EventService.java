package com.changecode.tintilinici.service;

import com.changecode.tintilinici.dto.EventDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

public interface EventService {
    
    Page<EventDTO> getByTagSorted(String tag, int page, int pageSize, String sortedBy, Sort.Direction direction);

    EventDTO getById(Long id);

    Long create(EventDTO eventDTO);

    //TODO other CRUD operations
}
