package com.changecode.tintilinici.service;

import com.changecode.tintilinici.dto.GroupDTO;

import java.util.List;

public interface GroupService {

    Long create(GroupDTO groupDTO);

    List<GroupDTO> getTopGroups();

    GroupDTO getById(Long id);

    List<GroupDTO> getGroupsFollowed();

    //TODO other CRUD operations
}
