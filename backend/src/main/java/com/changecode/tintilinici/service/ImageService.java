package com.changecode.tintilinici.service;

public interface ImageService {

    byte[] userProfile(Long userId);

    byte[] groupProfile(Long groupId);

    byte[] groupHeading(Long groupId);

    byte[] blog(Long blogId);
}
