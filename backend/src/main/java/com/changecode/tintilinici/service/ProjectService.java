package com.changecode.tintilinici.service;

import com.changecode.tintilinici.dto.ProjectDTO;
import org.springframework.data.domain.Page;

public interface ProjectService {

    Page<ProjectDTO> getByTagSorted(String tag, int page, int pageSize);

    Long create(ProjectDTO projectDTO);

    ProjectDTO getById(Long id);

    //TODO other CRUD operations
}
