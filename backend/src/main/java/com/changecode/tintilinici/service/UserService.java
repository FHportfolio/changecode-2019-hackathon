package com.changecode.tintilinici.service;

import com.changecode.tintilinici.dto.UserDTO;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    Long create(UserDTO user);

    //TODO other CRUD operations
}
