package com.changecode.tintilinici.service.impl;

import com.changecode.tintilinici.model.User;
import com.changecode.tintilinici.repository.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.function.Supplier;

public abstract class AuthorizedService {

    protected static final Supplier<RuntimeException> USERNAME_NOT_FOUND = () -> new UsernameNotFoundException("User with provided email has not been registered or hasn't verified his account.");

    private UserRepository userRepository;

    protected AuthorizedService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    protected User getActiveUserInternal() {
        return userRepository
                .findByEmail(SecurityContextHolder
                        .getContext()
                        .getAuthentication()
                        .getName()).orElseThrow(USERNAME_NOT_FOUND);

    }

}
