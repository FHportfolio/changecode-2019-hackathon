package com.changecode.tintilinici.service.impl;

import com.changecode.tintilinici.dto.BlogCommentDTO;
import com.changecode.tintilinici.dto.BlogDTO;
import com.changecode.tintilinici.dto.BlogReactionDTO;
import com.changecode.tintilinici.mapper.impl.BlogCommentMapper;
import com.changecode.tintilinici.mapper.impl.BlogMapper;
import com.changecode.tintilinici.model.Blog;
import com.changecode.tintilinici.model.BlogReaction;
import com.changecode.tintilinici.model.Tag;
import com.changecode.tintilinici.repository.BlogCommentRepository;
import com.changecode.tintilinici.repository.BlogReactionRepository;
import com.changecode.tintilinici.repository.BlogRepository;
import com.changecode.tintilinici.repository.TagRepository;
import com.changecode.tintilinici.repository.UserRepository;
import com.changecode.tintilinici.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.stream.Collectors;

@Service
public class BlogServiceImpl extends AuthorizedService implements BlogService {

    private final BlogRepository blogRepository;
    private final TagRepository tagRepository;
    private final BlogMapper blogMapper;
    private final BlogCommentMapper blogCommentMapper;
    private final BlogCommentRepository blogCommentRepository;
    private final BlogReactionRepository blogReactionRepository;

    @Autowired
    public BlogServiceImpl(UserRepository userRepository, BlogRepository blogRepository, BlogMapper blogMapper, TagRepository tagRepository, BlogCommentMapper blogCommentMapper, BlogCommentRepository blogCommentRepository, BlogReactionRepository blogReactionRepository) {
        super(userRepository);
        this.blogRepository = blogRepository;
        this.blogMapper = blogMapper;
        this.tagRepository = tagRepository;
        this.blogCommentMapper = blogCommentMapper;
        this.blogCommentRepository = blogCommentRepository;
        this.blogReactionRepository = blogReactionRepository;
    }

    @Override
    public BlogDTO getById(Long id) {
        return blogMapper.entityToDto(blogRepository.findById(id).orElseThrow(EntityNotFoundException::new));
    }

    @Override
    public Long create(BlogDTO blogDTO) {
        //TODO check if user is in group

        Blog blog = blogMapper.dtoToEntity(blogDTO);
        blog = blogRepository.save(blog);

        return blog.getId();
    }

    @Override
    public Page<BlogDTO> getByGroupIdSorted(Long groupId, int page, int pageSize, String sortedBy, Sort.Direction direction) {
        Pageable pageable = PageRequest.of(page, pageSize, direction, sortedBy);

        Page<Blog> blogPage = blogRepository.findAllByGroupId(groupId, pageable);

        return new PageImpl<>(blogPage.stream().map(blogMapper::entityToDto).collect(Collectors.toList()), pageable, blogPage.getTotalElements());
    }

    @Override
    public Page<BlogDTO> getByTagSorted(String tag, int page, int pageSize, String sortedBy, Sort.Direction direction) {
        Pageable pageable = PageRequest.of(page, pageSize, direction, sortedBy);

        if(tag != null){
            Tag tagEntity = tagRepository.findByTitle(tag).orElseThrow(EntityNotFoundException::new);
            Page<Blog> blogPage = blogRepository.findAllByTagsContaining(tagEntity, pageable);

            return new PageImpl<>(blogPage.stream().map(blogMapper::entityToDto).collect(Collectors.toList()), pageable, blogPage.getTotalElements());
        } else {
            Page<Blog> blogPage = blogRepository.findAll(pageable);

            return new PageImpl<>(blogPage.stream().map(blogMapper::entityToDto).collect(Collectors.toList()), pageable, blogPage.getTotalElements());
        }
    }

    @Override
    public Long addComment(BlogCommentDTO commentDTO) {
        return blogCommentRepository.save(blogCommentMapper.dtoToEntity(commentDTO)).getId();
    }

    @Override
    public Long addReaction(BlogReactionDTO blogReactionDTO) {
        Blog blog = blogRepository.findById(blogReactionDTO.getBlogId()).orElseThrow(EntityNotFoundException::new);

        BlogReaction blogReaction = new BlogReaction();
        blogReaction.setUpvote(blogReactionDTO.getIsUpvote());
        blogReaction.setBlog(blog);
        blogReaction.setUser(getActiveUserInternal());

        return blogReactionRepository.save(blogReaction).getId();
    }
}
