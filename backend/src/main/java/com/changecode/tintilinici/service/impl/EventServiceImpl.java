package com.changecode.tintilinici.service.impl;

import com.changecode.tintilinici.dto.EventDTO;
import com.changecode.tintilinici.mapper.impl.EventMapper;
import com.changecode.tintilinici.model.Event;
import com.changecode.tintilinici.model.Tag;
import com.changecode.tintilinici.repository.EventRepository;
import com.changecode.tintilinici.repository.TagRepository;
import com.changecode.tintilinici.repository.UserRepository;
import com.changecode.tintilinici.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.stream.Collectors;

@Service
public class EventServiceImpl extends AuthorizedService implements EventService {

    private final EventRepository eventRepository;
    private final EventMapper eventMapper;
    private final TagRepository tagRepository;

    @Autowired
    public EventServiceImpl(UserRepository userRepository,EventRepository eventRepository, TagRepository tagRepository, EventMapper eventMapper) {
        super(userRepository);

        this.eventRepository = eventRepository;
        this.tagRepository = tagRepository;
        this.eventMapper = eventMapper;
    }

    @Override
    public Page<EventDTO> getByTagSorted(String tag, int page, int pageSize, String sortedBy, Sort.Direction direction) {
        Pageable pageable = PageRequest.of(page, pageSize, direction, sortedBy);

        if(tag != null){
            Tag tagEntity = tagRepository.findByTitle(tag).orElseThrow(EntityNotFoundException::new);
            Page<Event> eventPage = eventRepository.findAllByFromDateAfterAndTagsContaining(new Date(), tagEntity, pageable);

            return new PageImpl<>(eventPage.stream().map(eventMapper::entityToDto).collect(Collectors.toList()), pageable, eventPage.getTotalElements());
        } else {
            Page<Event> eventPage = eventRepository.findAllByFromDateAfter(new Date(), pageable);

            return new PageImpl<>(eventPage.stream().map(eventMapper::entityToDto).collect(Collectors.toList()), pageable, eventPage.getTotalElements());
        }
    }

    @Override
    public EventDTO getById(Long id) {
        return eventMapper.entityToDto(eventRepository.findById(id).orElseThrow(EntityNotFoundException::new));
    }

    @Override
    public Long create(EventDTO eventDTO) {
        return eventRepository.save(eventMapper.dtoToEntity(eventDTO)).getId();
    }
}
