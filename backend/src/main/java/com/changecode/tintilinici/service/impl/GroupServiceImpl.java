package com.changecode.tintilinici.service.impl;

import com.changecode.tintilinici.dto.GroupDTO;
import com.changecode.tintilinici.dto.StemDTO;
import com.changecode.tintilinici.mapper.impl.GroupMapper;
import com.changecode.tintilinici.repository.GroupRepository;
import com.changecode.tintilinici.repository.UserRepository;
import com.changecode.tintilinici.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupServiceImpl extends AuthorizedService implements GroupService {

    private final GroupRepository groupRepository;
    private final GroupMapper groupMapper;

    @Autowired
    public GroupServiceImpl(UserRepository userRepository, GroupRepository groupRepository, GroupMapper groupMapper) {
        super(userRepository);

        this.groupRepository = groupRepository;
        this.groupMapper = groupMapper;
    }


    @Override
    public Long create(GroupDTO groupDTO) {
        //TODO check if user is in group
        return groupRepository.save(groupMapper.dtoToEntity(groupDTO)).getId();
    }

    @Override
    public List<GroupDTO> getTopGroups() {
        Sort sort = Sort.by("rating").descending();
        Pageable pageable = PageRequest.of(0, 10, sort);
        return groupRepository.findAll(pageable).stream().map(e -> {
            GroupDTO groupDTO = new GroupDTO();
            groupDTO.setRating(e.getRating());
            groupDTO.setName(e.getName());
            groupDTO.setId(e.getId());
            groupDTO.setProfileImage("/image/group/profile?groupId=" + e.getId());
            groupDTO.setFollowersCount(e.getFollowersCount());
            groupDTO.setMembersCount(e.getMembersCount());
            groupDTO.setDescription(e.getDescription());
            groupDTO.setStem(new StemDTO(e.getStemId(), e.getStem().getCode()));

            return groupDTO;
        }).collect(Collectors.toList());
    }

    @Override
    public GroupDTO getById(Long id) {
        return groupMapper.entityToDto(groupRepository.findById(id).orElseThrow(EntityNotFoundException::new));
    }

    @Override
    public List<GroupDTO> getGroupsFollowed() {
        return getActiveUserInternal().getGroupsFollowed().stream().map(groupMapper::entityToDto).collect(Collectors.toList());
    }
}
