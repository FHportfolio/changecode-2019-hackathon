package com.changecode.tintilinici.service.impl;

import com.changecode.tintilinici.repository.BlogRepository;
import com.changecode.tintilinici.repository.GroupRepository;
import com.changecode.tintilinici.repository.UserRepository;
import com.changecode.tintilinici.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;

@Service
public class ImageServiceImpl implements ImageService {

    private final UserRepository userRepository;
    private final GroupRepository groupRepository;
    private final BlogRepository blogRepository;

    @Autowired
    public ImageServiceImpl(UserRepository userRepository, GroupRepository groupRepository, BlogRepository blogRepository) {
        this.userRepository = userRepository;
        this.groupRepository = groupRepository;
        this.blogRepository = blogRepository;
    }

    @Override
    public byte[] userProfile(Long userId) {
        return userRepository.findById(userId).orElseThrow(EntityExistsException::new).getProfileImage();
    }

    @Override
    public byte[] groupProfile(Long groupId) {
        return groupRepository.findById(groupId).orElseThrow(EntityExistsException::new).getProfileImage();
    }

    @Override
    public byte[] groupHeading(Long groupId) {
        return groupRepository.findById(groupId).orElseThrow(EntityExistsException::new).getHeaderImage();
    }

    @Override
    public byte[] blog(Long blogId) {
        return blogRepository.findById(blogId).orElseThrow(EntityExistsException::new).getImage();
    }
}
