package com.changecode.tintilinici.service.impl;

import com.changecode.tintilinici.dto.ProjectDTO;
import com.changecode.tintilinici.mapper.impl.ProjectMapper;
import com.changecode.tintilinici.model.Project;
import com.changecode.tintilinici.model.Tag;
import com.changecode.tintilinici.repository.ProjectRepository;
import com.changecode.tintilinici.repository.TagRepository;
import com.changecode.tintilinici.repository.UserRepository;
import com.changecode.tintilinici.service.ProjectService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.stream.Collectors;

@Service
public class ProjectServiceImpl extends AuthorizedService implements ProjectService {

    private final ProjectRepository projectRepository;
    private final TagRepository tagRepository;
    private final ProjectMapper projectMapper;

    public ProjectServiceImpl(UserRepository userRepository, ProjectRepository projectRepository, TagRepository tagRepository, ProjectMapper projectMapper){
        super(userRepository);

        this.projectRepository = projectRepository;
        this.tagRepository = tagRepository;
        this.projectMapper = projectMapper;
    }

    @Override
    public Page<ProjectDTO> getByTagSorted(String tag, int page, int pageSize) {
        Pageable pageable = PageRequest.of(page, pageSize);

        if(tag != null){
            Tag tagEntity = tagRepository.findByTitle(tag).orElseThrow(EntityNotFoundException::new);
            Page<Project> projects = projectRepository.findAllByTagsContaining(tagEntity, pageable);

            return new PageImpl<>(projects.stream().map(projectMapper::entityToDto).collect(Collectors.toList()), pageable, projects.getTotalElements());
        } else {
            Page<Project> projects = projectRepository.findAll(pageable);

            return new PageImpl<>(projects.stream().map(projectMapper::entityToDto).collect(Collectors.toList()), pageable, projects.getTotalElements());
        }
    }

    @Override
    public Long create(ProjectDTO projectDTO) {
        return projectRepository.save(projectMapper.dtoToEntity(projectDTO)).getId();
    }

    @Override
    public ProjectDTO getById(Long id) {
        return projectMapper.entityToDto(projectRepository.findById(id).orElseThrow(EntityNotFoundException::new));
    }
}
