package com.changecode.tintilinici.service.impl;

import com.changecode.tintilinici.dto.UserDTO;
import com.changecode.tintilinici.mapper.impl.UserMapper;
import com.changecode.tintilinici.model.User;
import com.changecode.tintilinici.repository.UserRepository;
import com.changecode.tintilinici.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserServiceImpl extends  AuthorizedService implements UserService {

    private static final String SIMPLE_USER = "simple_user";
    private static final String ADMIN_USER = "admin_user";

    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper) {
        super(userRepository);
        this.userRepository = userRepository;
        this.userMapper = userMapper;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(username).orElseThrow(USERNAME_NOT_FOUND);

        Set<GrantedAuthority> auth = new HashSet<>();
        auth.add(user.getIsAdmin() ? new SimpleGrantedAuthority(ADMIN_USER):new SimpleGrantedAuthority(SIMPLE_USER));

        return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPasswordHash(), auth);
    }

    @Override
    public Long create(UserDTO user) {
        User userEntity = userMapper.dtoToEntity(user);
        userEntity = userRepository.save(userEntity);

        return userEntity.getId();
    }
}
