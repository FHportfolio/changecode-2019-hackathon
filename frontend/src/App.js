import React from 'react';
import './App.css';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import HomePage from "./homepage/HomePage";
import Header from "./header/Header";
import Post from "./posts/Post";
import Group from "./groups/Group";
import Event from "./events/Event";
import BlogDetails from "./blogDetails/BlogDetails";
import GroupDetails from "./groupDetails/groupDetails";
import Profile from "./profile/Profile";
import RegistrationForm from "./registrationForm/RegistrationForm";

function App() {
  return (
      <Header>
        <BrowserRouter>
            <Switch>
              <Route exact path="/" component={HomePage}/>
              <Route exact path="/post/" component={Post}/>
              <Route exact path="/group/" component={Group}/>
              <Route exact path="/event/" component={Event}/>
              <Route exact path="/post/details/:id?" component={BlogDetails}/>
              <Route exact path="/group/details/:id?" component={GroupDetails}/>
                <Route exact path="/profile" component={Profile}/>
                <Route exact path="/register" component={RegistrationForm}/>
              </Switch>
        </BrowserRouter>
      </Header>
  );
}

export default App;
