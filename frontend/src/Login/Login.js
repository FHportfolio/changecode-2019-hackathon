import React, { Component } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import axios from "axios";
import {LOGIN_URL} from "../utility/URL";
import queryString from "qs";


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: ''
        }
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleLogin() {
        axios({
            url: LOGIN_URL,
            method: 'post',
            headers: {...getAuthorizationHeader()},
            data: queryString.stringify({
                username:this.state.email,
                password:this.state.password,
                grant_type: 'password'
            })
        }).then(r => {
            console.log(r);
            saveToken(r.data);
            return r;
        })
            .catch(e => {
                throw e
            });
    }

    render() {
        return (
            <Form>
                <FormGroup>
                    <Label for="Email">Email</Label>
                    <Input value={this.state.email} type="email" name="email" id="email" placeholder="email" onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                    <Label for="Lozinka">Lozinka</Label>
                    <Input value={this.state.password} type="password" name="password" id="password" placeholder="lozinka" onChange={this.handleChange}/>
                </FormGroup>
                <Button onClick={() => this.handleLogin()}>Login</Button>
            </Form>
        );
    }
}
export default Login;

export const getAuthorizationHeader = () => {
    let token = getToken();
    console.log(token);
    console.log('ovo je bio token');
    if (token !== null && token !== undefined) {
        return {
            "Authorization": "Bearer " + token.access_token
        };
    }
    else {
        // default client and secret (client_id:secret)
        return {"Authorization": "Basic dXNlcjp1c2Vy"};
    }
};

export const getToken =  () => {
    return JSON.parse(localStorage.getItem(JWT_TOKEN));
};

export const isLoggedIn = () => {
    console.log(getToken());
    console.log('ovo je token');
    console.log(getAuthorizationHeader())
    return getToken() !== null && getToken() !== undefined;
};

export const saveToken = (token) => {
    localStorage.setItem(JWT_TOKEN, token);
};

export const removeToken = () => {
    localStorage.removeItem(JWT_TOKEN);
};


export const JWT_TOKEN = "jwt_token";
