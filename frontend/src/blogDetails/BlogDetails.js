import React, {Component} from 'react';
import {Badge, Button, Card, CardHeader, Col, Container, Form, FormGroup, Input, Row} from "reactstrap";

import CardBody from "reactstrap/es/CardBody";
import CardText from "reactstrap/es/CardText";
import CardFooter from "reactstrap/es/CardFooter";

export default class BlogDetails extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {
                firstName: "Jonny",
                lastName: "Stemovich",
            },
            title: "TITLE",
            body: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin condimentum dignissim orci, et sagittis velit dapibus non. Nulla porta leo mauris, ac elementum ante feugiat et. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vivamus lorem risus, maximus lobortis pulvinar ac, dictum in mi. Donec posuere malesuada turpis. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Maecenas volutpat augue sed varius hendrerit. Sed tincidunt consequat mauris a hendrerit. Nam lectus turpis, rhoncus et massa sit amet, auctor interdum diam. Proin semper ut diam eu malesuada. Nam lorem augue, vehicula molestie tempus ut, interdum id lectus. Nullam bibendum quam at tellus tempor consectetur. Nullam non tortor non nibh lacinia aliquet.\n" +
                "\n" +
                "Donec vitae mauris ullamcorper, vehicula quam ut, euismod nulla. Phasellus blandit eget nisl id tincidunt. Integer dignissim velit ac risus tincidunt lacinia. Quisque euismod quis diam nec dignissim. Vivamus sed rutrum nunc. Quisque id hendrerit sapien, at vestibulum magna. Phasellus et sem pretium magna accumsan interdum. Vestibulum in ex et leo placerat mollis dignissim vel dolor. Duis suscipit dui sed eleifend aliquam. Nullam non congue tortor, at vulputate sapien. Proin et felis lacus. Nunc sit amet finibus ante. Praesent vel diam nec sem vulputate condimentum. Curabitur quis varius nibh.",
            comments: ["kdkdkdd", "le[wepfp[we", "keopwfkeofk"],
            upvotes: 32
        };

    }

    //AXIOS OVDJE ZA GETANJE PO AJ DI JU IZ URLA

    render() {
        return (
            <div>
                <h1 className="text-center">{this.state.title}</h1>
                <Container>
                    <Row>
                        <Col>
                            <Card>
                                <CardHeader>Autor: {this.state.user.firstName} {this.state.user.lastName} <Badge color="success"> Podrzalo:{this.state.upvotes}</Badge></CardHeader>
                                <CardBody>
                                    <CardText>{this.state.body}</CardText>
                                </CardBody>
                                <CardFooter>03.02.2019</CardFooter>
                            </Card>
                        </Col>
                    </Row>
                    {this.state.comments.map(comment => (
                        <Row>
                            <Col>
                                <Card>
                                    <CardHeader>User1</CardHeader>
                                    <CardText>User user1 says: {comment}!</CardText>
                                    <CardFooter/>
                                </Card>
                            </Col>
                        </Row>

                    ))}
                    <Row>
                        <Col>
                            <Form>
                                <FormGroup>
                                    <Input type="textarea" placeholder="Komentiraj"
                                           />
                                </FormGroup>
                            </Form>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Button style={{marginRight:"2px"}}>Komentiraj</Button>
                            <Button color="success">Podrži</Button>

                        </Col>
                    </Row>

                </Container>
            </div>
        );
    }

}
