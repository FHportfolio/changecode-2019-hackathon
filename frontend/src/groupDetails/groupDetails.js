import React, {Component} from 'react';
import {Button, Col, Container, Row} from "reactstrap";
import EventCard from "../events/EventCard";
import PostCard from "../posts/PostCard";
import ProjectCard from "./ProjectCard";

export default class GroupDetails extends Component {
    data = [1, 2, 3];

    constructor(props) {
        super(props);

        this.state = {
            name: "Group Name"
        }

    }


    // AXIOS izvuci info iz url izvuci 3 eventa




    // + je za join group, ovaj S je jedan od kombinacija koda od stem objekta

    render() {
        return (
            <div className="bd">


                <Container>
                    <Row>
                        <Col><h1 className="text-center">{this.state.name}</h1></Col>
                        <Col>
                            <Button color="success" className="" style={{borderRadius: "50%",margin:"auto"}}>+</Button>
                            <Button style={{marginLeft:"1px", borderRadius:"50%"}} >S</Button>
                        </Col>
                    </Row>
                    <Row>
                        {this.data.map(n => (
                            <Col> <EventCard name={"nesto"}
                                             groupName={"grupa 01"}
                                             code={"STEM"}
                                             title={"Zasto je Lopoč?"}
                                             description={"ovo je neki probni tekst koji ce se prikazati jednog lijepog dana kada sve proradi iducih 20 rijeci \" +\n" +
                                             "                \"ce biti rijec : rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec \" +\n" +
                                             "                \"rijec rijec rijec rijec rijec "}
                                             comments={["jedan", "dva"]}
                            /></Col>
                        ))}
                    </Row>
                    <Row>
                        {this.data.map(n => (
                            <Col>
                                <PostCard name={"nesto"}
                                          groupName={"grupa 01"}
                                          code={"STEM"}
                                          title={"Zasto je Lopoč?"}
                                          description={"ovo je neki probni tekst koji ce se prikazati jednog lijepog dana kada sve proradi iducih 20 rijeci \" +\n" +
                                          "                \"ce biti rijec : rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec \" +\n" +
                                          "                \"rijec rijec rijec rijec rijec "}
                                          comments={["jedan", "dva"]}
                                />
                            </Col>

                        ))}
                    </Row>

                    <Row>
                        {this.data.map(n => (
                            <Col>
                                <ProjectCard name={"nesto"}
                                          groupName={"grupa 01"}
                                          code={"STEM"}
                                          title={"Zasto je Lopoč?"}
                                          description={"ovo je neki probni tekst koji ce se prikazati jednog lijepog dana kada sve proradi iducih 20 rijeci \" +\n" +
                                          "                \"ce biti rijec : rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec \" +\n" +
                                          "                \"rijec rijec rijec rijec rijec "}
                                          comments={["jedan", "dva"]}
                                />
                            </Col>

                        ))}
                    </Row>
                </Container>
            </div>
        )
    }


}
