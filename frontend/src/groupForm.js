import React, {Component} from "react";
import {Button, Form, FormGroup, Input, Label} from "reactstrap";
import axios from "axios";
import {CREATE_GROUP} from "./utility/URL";
import {getAuthorizationHeader} from "./Login/Login";

export default class GroupForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: '',
            profileImage:null,
            headerImage:null
        }
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleImageChange = async (event, imageType) => {
        console.log(event.target);
        await this.setState({
            [imageType]: event.target.files[0],
            error: ""
        });
    };

    handleSave(){
        console.log(this.state);
        if(this.state.image === null) {
            this.setState({error: "Image not selected"});
            return ;
        }
        let data = {
            name: this.state.name,
            description: this.state.description,
        };

        let frHeaderImage = new FileReader();
        frHeaderImage.onload = (headerImage) => {
            data.headerImage = [];
            (new Int8Array(headerImage.target.result)).forEach((value, index) => {
                data.headerImage.push(value);
            });
        };

        let frProfileImage = new FileReader();

        frProfileImage.onload = (profileImage) => {
            data.profileImage = [];
            (new Int8Array(profileImage.target.result)).forEach((value, index) => {
                data.profileImage.push(value);
            });
        };
        frProfileImage.readAsArrayBuffer(this.state.profileImage);
        frHeaderImage.readAsArrayBuffer(this.state.headerImage);
        // axios
        axios({
            url: CREATE_GROUP,
            method: 'POST',
            data: data,
            headers: getAuthorizationHeader()
        }).then(r => {
            console.log(r);
        })
    };


    render() {
        return (
            <Form>
                <FormGroup>
                    <Label for="Name">Ime</Label>
                    <Input value={this.state.name} type="text" name="name" id="name" placeholder="Ime" onChange={this.handleChange} />
                </FormGroup>
                <FormGroup>
                    <Label for="Description">Opis</Label>
                    <Input value={this.state.description} type="text" name="description" id="description" placeholder="Opis" onChange={this.handleChange}/>
                </FormGroup>
                <input type='file' id='profileImage' onChange={(event) => this.handleImageChange(event, 'profileImage')} multiple/>
                <input type='file' id='headerImage' onChange={(event) => this.handleImageChange(event, 'headerImage')} multiple/>
                <br/>
                <Button className="left" onClick={() => this.resetFields()}>Cancel</Button>
                <Button onClick={() => this.handleSave()}>Create group</Button>
            </Form>
        );
    }

    resetFields() {
        this.setState({name:'', description:'', profileImage:null, headerImage:null});
    }
}