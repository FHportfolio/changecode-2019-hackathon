import React, {Component} from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import Container from "reactstrap/es/Container";
import GroupCard from "./GroupCard";

export default class Group extends Component {
    state = {
        items: Array.from({length:100}),
        hasMore: true
    };

    constructor(props) {
        super(props);
    }

    fetchMoreData = () => {
        if (this.state.items.length >= 200) {
            this.setState({ hasMore: false });
            return;
        }
        // a fake async api call like which sends
        // 20 more records in .5 secs
        setTimeout(() => {
            this.setState({
                items: this.state.items.concat(Array.from({ length: 50 }))
            });
        }, 200);
    };

    render() {
        return (
            <div className="Posts">
                <Container>
                    <InfiniteScroll
                        dataLength={this.state.items.length}
                        next={this.fetchMoreData}
                        hasMore={this.state.hasMore}
                        loader={<h4>Loading...</h4>}
                        endMessage={
                            <p style={{ textAlign: "center" }}>
                                <b>Yay! You have seen it all</b>
                            </p>
                        }
                    >

                        {this.state.items.map((i, index) => (
                            <GroupCard name={"nesto"}
                                      groupName={"grupa 01"}
                                      code={"STEM"}
                                      title={"Zasto je Lopoč?"}
                                      description={"ovo je neki probni tekst koji ce se prikazati jednog lijepog dana kada sve proradi iducih 20 rijeci \" +\n" +
                                      "                \"ce biti rijec : rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec \" +\n" +
                                      "                \"rijec rijec rijec rijec rijec "}
                                      comments={["jedan","dva"]}
                            />                        ))}
                    </InfiniteScroll>
                </Container>
            </div>
        );
    }
}