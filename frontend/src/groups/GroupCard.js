import React from 'react';
import {Button, Card, CardHeader, CardText, CardTitle, Col, Row} from 'reactstrap';

const GroupCard = (props) => {

    return (
        <Row>
            <Col>
                <Card body>
                    <CardHeader>{props.name}</CardHeader>
                    <CardTitle>{props.groupName} {props.code}</CardTitle>
                    <CardText>{props.title}</CardText>
                    <CardText>{props.description}</CardText>

                    <Button>See more</Button>
                </Card>
            </Col>
        </Row>
    );
};

export default GroupCard;