import React, {Component} from "react";
import './header.css';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink, Modal, ModalHeader, ModalBody, Button
} from 'reactstrap';
import SearchBar from '@opuscapita/react-searchbar';
import {isLoggedIn, removeToken} from "../Login/Login";
import Login from "../Login/Login";

export default class Header extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
            isLoggedIn: false,
            modal:false,
            logout:false
        };
    }

    componentDidMount() {
        this.setState({isLoggedIn: isLoggedIn()})
    }

    toggle() {
        this.setState({
            modal: !this.state.modal
        });
    }

    handleSearch = () => {
      alert("")
    };

    render() {
        return (
            <div>
                <Navbar color="light" light expand="md">
                    <NavbarBrand href="/">stemalica</NavbarBrand>
                    <NavbarToggler onClick={this.toggle} />
                    <Collapse isOpen={() => this.state.isOpen()} navbar>
                        <Nav className="mr-auto" navbar>
                            <NavItem>
                                <NavLink href="/post">Objave</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/group">Grupe</NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/event">Događaji</NavLink>
                            </NavItem>
                        </Nav>

                        <Nav className="ml-auto" navbar>
                            <NavItem>
                                <SearchBar
                                    onSearch={this.handleSearch}
                                />
                            </NavItem>
                            {this.state.isLoggedIn ? (
                                <div>
                                    <NavItem>
                                        <Button outline color="primary" onClick={() => this.logout()}>Log Out</Button>
                                    </NavItem>
                                <NavItem>
                                <NavLink href="/profile">Moj profil</NavLink>
                            </NavItem>
                                </div>
                            ) : (
                                <NavItem>
                                        <Button outline color="primary" onClick={() => this.toggle()}>Log In</Button>
                                </NavItem>
                            )}

                        </Nav>
                    </Collapse>
                </Navbar>
                {this.props.children}
                <Modal isOpen={this.state.modal} toggle={() => this.toggle()}>
                    <ModalHeader toggle={() => this.toggle()}>Modal title</ModalHeader>
                    <ModalBody>
                        <Login/>
                    </ModalBody>
                </Modal>
            </div>
        );
    }
}