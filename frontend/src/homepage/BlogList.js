import React from "react";
import {Container} from "reactstrap";
import PostCard from '../posts/PostCard';
import axios from "axios";
import {GET_BLOGS_PAGEABLE} from "../utility/URL";

export default class BlogList extends React.Component {
    constructor(props) {
        super();

        this.state = {
            tags:this.props === undefined ? undefined : this.props.tags,
            blogs: [{
                id:0,
                image: null,
                title: 'Naslov',
                created: '1/2/2019',
                upVotesCount: 1000,
                downVotesCount: 35,
                comments: [],
                body:'aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa '
            },
                {
                    id:1,
                    image: null,
                    title: 'Naslov',
                    created: '1/2/2019',
                    upVotesCount: 1000,
                    downVotesCount: 35,
                    comments: [],
                    body:'aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa aaaa '
                }]
        }
    }

    componentDidMount() {
        if(this.props.tags === undefined) {
            // axios za sve
            axios({
                url: GET_BLOGS_PAGEABLE + '?page=0&size=10',
                contentType: 'application/json',
                method: 'get'
            }).then(r => {
                console.log(r.data);
                this.setState({projects: r.data.content});
            })
        }
        else {
        //    axios po tagovima
            axios({
                url: GET_BLOGS_PAGEABLE + '?tag=' + this.state.tags +'&page=0&size=10',
                contentType: 'application/json',
                method: 'get'
            }).then(r => {
                console.log(r.data);
                this.setState({projects: r.data.content});
            })
        }
    }

    render() {
        return (
            <Container>
                {this.state.blogs.slice(0,5).map((event, index) => (
                    <PostCard name={"nesto"}
                              groupName={"grupa 01"}
                              code={"STEM"}
                              title={"Zasto je Lopoč?"}
                              description={"ovo je neki probni tekst koji ce se prikazati jednog lijepog dana kada sve proradi iducih 20 rijeci \" +\n" +
                              "                \"ce biti rijec : rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec \" +\n" +
                              "                \"rijec rijec rijec rijec rijec "}
                              comments={["jedan","dva"]}
                    />
                ))}
                {this.props.children}
                {this.state.blogs.slice(5,this.state.blogs.length).map((event, index) => (
                    <PostCard name={"nesto"}
                              groupName={"grupa 01"}
                              code={"STEM"}
                              title={"Zasto je Lopoč?"}
                              description={"ovo je neki probni tekst koji ce se prikazati jednog lijepog dana kada sve proradi iducih 20 rijeci \" +\n" +
                              "                \"ce biti rijec : rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec rijec \" +\n" +
                              "                \"rijec rijec rijec rijec rijec "}
                              comments={["jedan","dva"]}
                    />                ))}
            </Container>
        );
    }
}
