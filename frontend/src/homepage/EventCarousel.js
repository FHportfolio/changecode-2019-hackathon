import React from "react";
import {Card, CardBody, CardText, CardTitle, Col, Row} from "reactstrap";
import {ButtonBack, ButtonNext, CarouselProvider, Slide, Slider} from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';
import './homepage.css'
import axios from "axios";
import {GET_EVENTS_PAGEABLE, host} from "../utility/URL";

export default class EventCarousel extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tags:this.props === undefined ? undefined : this.props.tags,
            currentSlide: 0,
            events: [{}, {}, {}, {}]
        };
    }

    componentDidMount() {
        if(this.props.tags === undefined) {
            // axios za sve
            axios({
                url: GET_EVENTS_PAGEABLE + '?page=0&size=10',
                contentType: 'application/json',
                method: 'get'
            }).then(r => {
                console.log(r.data);
                this.setState({events: r.data.content});
            })
        }
        else {
            //    axios po tagovima
            axios({
                url: GET_EVENTS_PAGEABLE + '?tag=' + this.state.tag + '&page=0&size=10',
                contentType: 'application/json',
                method: 'get'
            }).then(r => {
                console.log(r.data);
                this.setState({events: r.data.content});
            })
        }
    }

    getCarouselRound(round) {
        return (
            <Row>{
                this.state.events.slice(round, round + 1).map((project, index) => {
                    return (
                        <Col key={index}>
                            <Card>
                                <img top width="70" src={host + project.headerImage} alt="Card image cap"/>
                                <CardBody>
                                    <CardTitle>{project.name}</CardTitle>
                                    <CardText>{project.description}</CardText>
                                </CardBody>
                            </Card>
                        </Col>
                    )
                })}
            </Row>
        )
    }

    componentDidMount() {
        if(this.props.tags === undefined) {
            // axios za sve
        }
        else {
            //    axios po tagovima
        }
    }

    render() {
        let counter = [];
        for(let i = 0; i < Math.ceil(this.state.events.length/3); ++i){
            counter.push(i)
        }

        return (
            <CarouselProvider
                naturalSlideWidth={100}
                naturalSlideHeight={125}
                totalSlides={counter.length}
                currentSlide={this.state.currentSlide}
            >
                <Slider>
                    {counter.map((value, index) => {
                        return <Slide key={index} index={index}>{this.getCarouselRound(index)}</Slide>
                    })}
                </Slider>
                <ButtonBack>Nazad</ButtonBack>
                <ButtonNext>Iduci</ButtonNext>
            </CarouselProvider>
        );
    }
}