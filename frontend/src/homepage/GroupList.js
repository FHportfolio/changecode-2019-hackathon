import React from "react";
import {Card, CardBody, CardText, Col, Container, Row} from "reactstrap";
import axios from "axios";
import {GET_GROUPS} from "../utility/URL";

export default class GroupList extends React.Component {
    constructor(props) {
        super();
        this.state = {
            groups: []
        }
    }

    componentDidMount() {
        // axios za sve
        axios({
            url: GET_GROUPS,
            contentType: 'application/json',
            method: 'get'
        }).then(r => {
            console.log(r.data.content);
            this.setState({groups: r.data});
        })

    }

    createCard(group) {
        return (
            <Row>
                <Col>
                <Card>
                    <img width={"70px"} src={group.profileImage} alt="groupImage"/>
                    <CardBody>
                        {group.name}
                        <br/>
                        Members: {group.membersCount} Followers:{group.followersCount}
                        <Row className="border-top">
                            <CardText className="p-1">
                                {group.description.length < 150 ? group.description : group.description.substr(0, 150) + '...more'}
                            </CardText>
                        </Row>
                    </CardBody>
                </Card>
                </Col>
            </Row>
        )
    }

    render() {
        console.log(this.state.groups);
        return (
            <Container>
                {this.state.groups.slice(0,3).map((group, index) => {
                    return this.createCard((group))
                })}
                    {this.props.children}

                {this.state.groups.slice(3,this.state.groups.length).map((group, index) => {
                    return this.createCard((group))
                })}
            </Container>
        );
    }
}