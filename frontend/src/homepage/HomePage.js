import React from "react";
import {Col, Container, Row} from 'reactstrap';
import './homepage.css';
import BlogList from "./BlogList";
import EventCarousel from "./EventCarousel";
import GroupList from "./GroupList";
import ProjectCarousel from "./ProjectCarousel";


export default class HomePage extends React.Component {
    constructor() {
        super();
        this.state = {
            loggedIn: undefined,
            blogs: [],
            events: [],
            projects: [],
            groups: []
        };
    }
    componentDidMount() {
    //    axios to check login ili nesto pa dohvat stvari ovisno o logged in ili ne
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col sm="9">
                        <BlogList>
                            <EventCarousel/>
                        </BlogList>
                    </Col>
                    <Col sm="3">
                        <GroupList>
                            <ProjectCarousel/>
                        </GroupList>
                    </Col>
                </Row>
            </Container>
        )
    }
}