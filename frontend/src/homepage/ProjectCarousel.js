import React from "react";
import {Card, CardBody, CardText, CardTitle, Col, Row} from "reactstrap";
import {ButtonBack, ButtonNext, CarouselProvider, Slide, Slider} from "pure-react-carousel";
import './homepage.css';
import axios from "axios";
import {GET_PROJECTS_PAGEABLE, host} from "../utility/URL";

export default class ProjectCarousel extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            tags:this.props === undefined ? undefined : this.props.tags,
            projects: []
        };
    }

    getCarouselRound(round) {
        return (
                <Row>{
                    this.state.projects.slice(round, round + 1).map((project, index) => {
                        return (
                            <Col key={index}>
                                <Card>
                                    <img top width="70" src={host + project.headerImage} alt="Card image cap"/>
                                    <CardBody>
                                        <CardTitle>{project.name}</CardTitle>
                                        <CardText>{project.description}</CardText>
                                    </CardBody>
                                </Card>
                            </Col>
                        )
                    })}
                </Row>
        )
    }

    componentDidMount() {
        if(this.props.tags === undefined) {
            // axios za sve
            axios({
                url: GET_PROJECTS_PAGEABLE + '?page=0&size=10',
                contentType: 'application/json',
                method: 'get'
            }).then(r => {
                console.log(r.data);
                this.setState({projects: r.data.content});
            })
        }
        else {
            //    axios po tagovima
            axios({
                url: GET_PROJECTS_PAGEABLE + '?tag=' + this.state.tag + '&page=0&size=10',
                contentType: 'application/json',
                method: 'get'
            }).then(r => {
                console.log(r.data);
                this.setState({projects: r.data.content});
            })
        }
    }

    render() {
        //https://github.com/express-labs/pure-react-carousel
        return (
            <Row>
                <Col>
            <CarouselProvider
                naturalSlideWidth={100}
                naturalSlideHeight={125}
                totalSlides={this.state.projects.length}
            >
                <Slider>
                    {this.state.projects.map((value, index) => {
                        return <Slide key={index} index={index}>{this.getCarouselRound(index)}</Slide>
                    })}
                </Slider>
                <ButtonBack >Nazad</ButtonBack>
                <ButtonNext>Iduci</ButtonNext>
            </CarouselProvider>
                </Col>
            </Row>
        );
    }
}