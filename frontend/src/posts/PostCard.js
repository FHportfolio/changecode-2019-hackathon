import React from 'react';
import {Button, Card, CardHeader, CardText, CardTitle, Col, Container, Row} from 'reactstrap';

const PostCard = (props) => {

    let comments = props.comments.length ===0 ? "Komentari(0)" :props.comments.map(comment => <CardText className="border-bottom">{comment}</CardText>);
    return (
        <Row>
            <Col>
                <Card body>
                    <CardHeader>{props.name}</CardHeader>
                    <CardTitle>{props.groupName} {props.code}</CardTitle>
                    <CardText>{props.title}</CardText>
                    <CardText>{props.description}</CardText>
                    <Card>
                        {comments}
                    </Card>
                    <Container><Row><Button color="success">Podrži</Button><Button>Pročitaj više</Button></Row></Container>
                </Card>
            </Col>
        </Row>
    );
};

export default PostCard;