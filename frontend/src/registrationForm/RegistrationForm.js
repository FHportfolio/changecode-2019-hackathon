import React, {Component} from 'react';
import {Button, Col, Container, Form, FormGroup, Input, Label, Row} from 'reactstrap';

export class RegistrationForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: this.props.register,
            fName : "",
            lName : "",
            username: "",
            email: "",
            password: "",
            confirmPassword:"",
            redirect:"",
            errors: {
                firstName: "",
                lastName: "",
                name: "",
                password: ""

            },
            visibleRegistrationErrors: false
        }
    }


    validate(fName,lName,email, password,confirmedPassword) {
        // true means invalid, so our conditions got reversed
        const emailRegex = '';
        return {
            firstName : fName.length != 0 ? '' : 'Ime ne smije biti prazno!',
            lastName : lName.length != 0 ? '' : 'Prezime ne smije biti prazno!',
            name: email.match(emailRegex) != null ? '' : 'Mail nije važeći!',
            password: password === confirmedPassword && password.length != 0 ? '' : 'Lozinke se ne poklapaju!'


        };
    }



    isValid(){
        const {modal, fName, lName, username, email, password, confirmPassword } = this.state;
        const errors = this.validate(this.state.fName,this.state.lName,this.state.email, this.state.password,this.state.confirmPassword);

        console.log("mail correct",errors.email)

        this.setState({errors: errors});

        return errors.email === '' && errors.password === '' && errors.firstName === '' && errors.lastName === '';

    }

    register() {
        let data = {
            name: this.state.fName,
            surname: this.state.lName,
            email: this.state.email,
            password: this.state.password
        };
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    render() {
        return (
            <Container>
                <Row>
                    <Col className="offset-2 col-md-8">
                      <Form>
                        <FormGroup>
                          <Label for="firstName">Ime</Label>
                          <Input type="text" name="fName" id="fname" placeholder="Ime" value={this.state.fName} onChange={(event) => this.handleChange(event)}></Input>
                        </FormGroup>
                        <FormGroup>
                          <Label for="lastName">Prezime</Label>
                          <Input type="text" name="lName" id="lname" placeholder="Prezime"  value={this.state.lName} onChange={(event) => this.handleChange(event)}></Input>

                        </FormGroup>
                        <FormGroup>
                          <Label for="exampleEmail">Email</Label>
                          <Input type="email" name="email" id="exampleEmail" placeholder="Email"  value={this.state.email} onChange={(event) => this.handleChange(event)} />

                        </FormGroup>
                        <FormGroup>
                          <Label for="examplePassword">Lozinka</Label>
                          <Input type="password" name="password" id="password" placeholder="Lozinka" value={this.state.password} onChange={(event) => this.handleChange(event)}/>

                        </FormGroup>
                          <FormGroup>
                              <Label for="examplePassword">Ponovljena lozinka</Label>
                              <Input type="password" name="confirmPassword" id="confirmedPassword" placeholder="Ponovljena lozinka"  value={this.state.confirmPassword} onChange={(event) => this.handleChange(event)}/>
                          </FormGroup>
                          {Object.entries(this.state.errors).map(([key, value]) => {
                              return value !== '' ? <div className="alert-danger">{value}</div> : <div/>
                                  })}

                        <Button onClick={() => this.isValid()}>Registracija</Button>
                      </Form>
                    </Col>
                </Row>
            </Container>
        );
      }
}

export default RegistrationForm;