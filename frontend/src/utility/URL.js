export const host = 'http://localhost:8080';

export const BLOG_CONTROLLER = host + "/auth/blog";
export const CREATE_BLOG = BLOG_CONTROLLER + "/create";
//blogDTO
export const ADD_BLOG_COMMENT = BLOG_CONTROLLER + "/comment/create";
//blogCommentDTO
export const REACT_TO_BLOG = BLOG_CONTROLLER + "/react";
//blogReactionDTO

export const EVENT_CONTROLLER = host + "/auth/event";
export const CREATE_EVENT = EVENT_CONTROLLER + "/create";
//eventDTO

export const GROUP_CONTROLLER = host+ "/auth/group";
export const CREATE_GROUP = GROUP_CONTROLLER + "/create";
//groupDTO

export const IMAGE_CONTROLLER = host + "/image";
export const USER_IMAGE = IMAGE_CONTROLLER + "/user";
export const GROUP_PROFILE_IMAGE = IMAGE_CONTROLLER + "/group/profile";
export const GROUP_HEADING_IMAGE = IMAGE_CONTROLLER + "/group/heading";
export const BLOG_IMAGE = IMAGE_CONTROLLER + "/blog";

export const PROJECT_CONTROLLER = host + "/auth/project";
export const CREATE_PROJECT = PROJECT_CONTROLLER + "/create";
//projectDTO

export const PUBLIC_CONTROLLER = host + "/public";
export const GET_EVENTS_PAGEABLE = PUBLIC_CONTROLLER + "events";
//tag, page, size
export const GET_BLOGS_PAGEABLE = PUBLIC_CONTROLLER + "/blogs";
//tag, page, size
export const GET_PROJECTS_PAGEABLE = PUBLIC_CONTROLLER + "/projects";
//tag, page, size
export const GET_GROUPS = PUBLIC_CONTROLLER + "/groups";
//nista
export const GET_EVENT_BY_ID = PUBLIC_CONTROLLER + "/event";
//id
export const GET_PROJECT_BY_ID = PUBLIC_CONTROLLER + "/project";
//id
export const GET_BLOG_BY_ID = PUBLIC_CONTROLLER + "/blog";
//id

export const LOGIN_URL = host + "/oauth/token";